'''
Created on 29 nov. 2020

@author: Cesar
'''
import os.path as path
from random import randint


'''class QuickSort:    == No funciono crear un objeto entonces tuve que meterlo como metodo'''
    
''' Lo primero, metodos para el archivo,  '''
class Archivo:
    '''Use este metodo porque me parecio el mas rapido segun el analisis'''
    def quicksort(self,numeros,izq,der):
        pivote = numeros[izq]
        i = izq
        j = der
        aux = 0
        while(i<j):
            while(numeros[i]<=pivote and i <j):
                i = i +1
            while(pivote < numeros[j]):
                j = j-1
            if(i<j):
                aux = numeros[i]
                numeros[i] = numeros[j]
                numeros[j] = aux
        numeros[izq] = numeros[j]
        numeros[j] = pivote
        if(izq < j-1):
            self.quicksort(numeros, izq, j-1)
        if(j+1<der):
            self.quicksort(numeros, j+1, der)
        return numeros
            
    direccion='archivoAuxiliar.txt'
    def verificarVacio(self):
        palabras=''
        '''Verificamos que el archivo exista por medio de la ruta'''
        if(not path.exists(self.direccion)):
            print("El archivo no existe, proceda a llenar el archivo")
            ##Metodo de llenado de archivo, en caso de no existir archivo
            self.llenar()
        archivo=open(self.direccion,"r")
        palabras=archivo.read()
        archivo.close()
        if(palabras==''):
            return True
        else:
            return False
        
    def borrarArchivo(self):
        import os
        ##importe os, porque es facilita saber si el archivo existe no
        if(os.path.exists(self.direccion)):
            os.remove(self.direccion)
            
            crear=open(self.direccion,'w')##De Write
            crear.close()
            
            
            print("El archivo fue borrado y restablecido en limpio con exito!")
        else:
            print("El archivo no se puede borrar, porque le archivo no existe, se creara uno nuevo")
            crear=open(self.direccion,'w')
            crear.close()
            print("Archivo creado con exito!") 
    
        
    def verificar(self):
        palabras=""
        if(not path.exists(self.direccion)):
            print("El archivo no existe, proceda a llenar el archivo")
            ##;Metodo de llenaod de archivo en caso de no existir
            self.llenar()
        archivo=open(self.direccion,"r")##De read
        palabras=archivo.read()
        archivo.close()
        separador=0
        i=0
        for i in range(i,len(palabras)):
            if(palabras[i]==","):
                separador=separador+1
        if(separador>0):
            print(f"Hay {separador} palabras en el archivo")
        elif(len(palabras)==0):
            print("Archivo Vacio! no tiene palabras")
        elif (separador==0):
            print(f"Hay {(separador+1+1)} palabras en el archivo")
    
    ##Metodo de llenado
   
    
    def llenar(self):
        print("ESTA SERA LA NUEVA SELECION DE PALABRAS ")
        print("Ingresa las palabras ***CORRECTAMENTE**")
        print("Ejemplo: Programacion,Estructura,Datos")
        palabrasE=input().upper().replace(" ","")
        
        ##Falta escoger el metodo de ordenamiento
        vAuxiliar1 = palabrasE.split(",")
        
        vectorE=self.quicksort(vAuxiliar1,0,len(vAuxiliar1)-1)
       
        separador=0
        palabrasS=""
        while len(vectorE)>=1:
            palabrasS=palabrasS+vectorE.pop(0)+","
        palabrasS=palabrasS+"|"
      
        cont=0
        j=0
        print(palabrasS)
        for j in range(j,len(palabrasS)):
            if(palabrasS[j]=="|"):
                cont=cont+1
        if(cont==1):
            archivo=open(self.direccion,"w")
            archivo.write(palabrasS)
            archivo.close()
        else:
            print("Falto el Separador")
        
          
    def obtenerPalabrasArchivo(self):
        if(self.verificarVacio()==False):
            palabras=""
            archivo=open(self.direccion,"r")
            palabras=archivo.read()
            archivo.close()
            return palabras
        else:
            return 1


class Oportunidades:
    def __init__(self):
        self.oportunidades=10
    def restarOportunidad(self):
        self.oportunidades=self.oportunidades-1
        
        
class PilaLetrasIngresadas:
    def __init__(self):
        self.pila=[]
        self.cima=-1
    
    def insertar(self,l):
        self.cima=self.cima+1
        self.pila.append(l)
    def obtenerLetras(self):
        return self.pila.copy()
    
class JuegoAhorcado(Archivo,Oportunidades):
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Eliga por favor solo un numero que sea 1,2,3,4 o 5 para salir")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Eliga por favor solo un numero 1,2,3,4 o 5 para salir")
                    e=1
        return r
    
    def cargarPalabra(self,palabras):
        palabrasSelec = palabras.split("|")
        return palabrasSelec

    
    
    def elegirPalabra(self,words):
        dividirPalabras = words.split("|")
        palabrasE = dividirPalabras[0]
        worki = []
       
        palabrasEspañol = palabrasE.split(",")
        rango = len(palabrasEspañol)-2
        aleatorio = randint(0,rango)
        worki.append(palabrasEspañol[aleatorio])
        worki.append("1")
       
        
        
        
        return worki
            
            
    def letrasIngresadas(self,conjuntoLetras):
        k = 0
        c = ""
        for k in range(0,len(conjuntoLetras)):
            c = c + conjuntoLetras[k]+"|"
        arregloLetras = c.split("|")
        m = self.quicksort(arregloLetras, 0,len(arregloLetras)-1)    
        return m     

   
            
            
            
    def obtenerLetras(self):
        return self.pilaLetras.copy()
    
    
    def busqueda(self,vector,letra):
        o=0
        c=len(vector)-1
        
        while o<=c:
            indicador=(o+c)//2
            if letra==vector[indicador]:
                return 1
            elif letra > vector[indicador]:
                o=indicador+1
            else:
                c=indicador-1
        return 0
    
    def ordenadorDeLetras(self,letras):
        cadena = ""
        x = 0
        for x in range(0,len(letras)):
            cadena = cadena+letras[x]+"-"
            x=x+1
        arregloLetras = cadena.split("-")
        arregloOtro = self.quicksort(arregloLetras, 0,len(arregloLetras)-1)
     
        
        return arregloOtro


    def obtenerSecreta(self,palabra,ingresadas):
        invalidos="!#$%&/()=?�'<>-_.:,;}{~+*"
        seccion=[]
       
        for a in range(0,len(palabra)):
            seccion.append(invalidos[a])
       
        i=0
        for i in range(0,len(ingresadas)):
            for x in range(0,len(palabra)):
                ##A char
                if(chr(ingresadas[i])==palabra[x].lower()):
                    m=seccion[x]
                    seccion[x]=chr(ingresadas[i])
            
        acomuladorChar=""
        for i in range(0,len(seccion)):
            ##Limites para las letras en ascii
            if(ord(seccion[i])>=97 and ord(seccion[i])<=122):
                acomuladorChar=acomuladorChar+seccion[i]
            else:
                acomuladorChar=acomuladorChar+"-"
        return acomuladorChar
    
    
    def adivinada(self,palabraSecreta,ingresadas):
        
        l=ingresadas.pila
        ##remplazar espacios
        palabraSecreta=palabraSecreta.replace(" ","")
        l=self.ordenadorDeLetras(l)
        t=len(palabraSecreta)
        x=0
        if len(palabraSecreta)>0:
            for i in range(0,len(palabraSecreta)):
                indice=palabraSecreta[i]
                for j in range(0,len(l)):
                    if indice==l[j]:
                        x=x+1
        else:
            ##en caso de no cumplir lo anterior se retornada un False, diciendo que la 
            ##Palabra no se adivino
            return False
        return x==t

    def inicioAhorcado(self,palabraX):
        v = []
        ingresadas = PilaLetrasIngresadas()
        win = False
        palabraSecreta = palabraX[0]
        lSelec = palabraX[1]
        oportunidades = Oportunidades
        p = ""
        if(len(palabraSecreta)>0):
            print(f"La palabra Secreta tine {len(palabraSecreta)} letras")
            print("¿Puedes adivinar cual es?")
            
            
            
            print("==================================================================")
        else:
            print("Debes llenar de forma correcta el archivo")
            self.llenar()
        op = Oportunidades()
        while((win == False and op.oportunidades>0) and len(palabraSecreta)>0):
            indie = 0
            v = []
            for i in ingresadas.pila:
                v.append(ord(i))
            print(f"oportunidades restantes = {op.oportunidades} ")
            print(f"Letras Disponibles: {self.abecedarioLetras(v).upper()}")
            print("Ingrese una letra:")
            ingresada = input().lower()
            print("==================================================================0")
            v =[]
            if (len(ingresada)==0):
                ingresada = "@"
            for m in ingresadas.pila:
                v.append(ord(m))
            lll = self.abecedarioLetras(v)
            siLetra = 0
            
            
            ordenadas = self.ordenadorDeLetras(lll)
            siLetra = self.busqueda(ordenadas,ingresada)
            ##siLetras debe contener un metodo de busqueda, dentro del vector de letras disponibles
            
            if(siLetra>0):
                ingresadas.insertar(ingresada)
                
                
                
                for w in range(0,len(palabraSecreta)):
                    if(ingresada==palabraSecreta[w].lower()):
                        indie = indie +1
                ##Si el indicador queda en 0 quiere decir que la letra ingresada no estana en 
                ##la palabra
                if(indie==0):
                    print("Esa letra no es, intenta con otra")
                    op.restarOportunidad()
                    v = [] ##Vaciar
                    for z in ingresadas.pila:
                        v.append(ord(z))
                else:
                    
                    v =[]
                    for i in ingresadas.pila:
                        v.append(ord(i))
                p = self.obtenerSecreta(palabraSecreta, v)
                print(f"Palabra: {p.upper()}")
            else:
                ##funcion que nos dira si la letra es un digito novalido
                if(ingresada.isdigit() or (not(ingresada.isalpha()))):
                    print("Esa nisiquiera es una letra...")
                else:
                    print("No te distraigas, esa letra ya la habias ingresado")
                print("Se que puedes hacerlo mejor")
                print("===============================================================")
            ##Aqui sabremos si la palabra fue adivinada o no
            if(self.adivinada(p, ingresadas)):
                    win = True
            else:
                win = False    
                
        if(win==True):
            print("Bien hecho, has adivinado la palabra secreta, eres muy ágil")
            print(f"{palabraSecreta}")
        ##Por otro lado al llegar las vidas a 0, pierdes
        elif(op.oportunidades==0):
            print(f"Estuviste muy cerca! pero te has quedado sin oportunidades")
            print(f"la palabra que no pudiste adivinar era: {palabraSecreta}")
                    
        
    def abecedarioLetras(self,letrasIngresadas):
    
        abcdebet=[]
        abc='abcdefghijklmnñopqrstuvwxyz' ## añadi la ñ pero no se si es correcto añadirla
        for i in range(0,len(abc)):
            abcdebet.append(ord(abc[i]))
            
            
        if(len(letrasIngresadas)>0):
            for j in range(0,len(abcdebet)):
                indice=abcdebet[j]
                
                for k in range(0,len(letrasIngresadas)):
                   
                    if(indice==letrasIngresadas[k]):
                        abcdebet[j]=128
                        ##relacion 1/2
        ##En char
        abc=''
        
        for l in range(0,len(abcdebet)):
            if(abcdebet[l]==128):
                abc= abc+"" ##relacion 2/2
            else:
                abc=abc+chr(abcdebet[l])
                       
        return abc
       
opcion =0
a = Archivo()
ja = JuegoAhorcado()
while(opcion!=5):
    print("==================== MENU ====================")
    print("Digite 1 para Verificar El archivo")
    print("Digite 2 para Ingresar palabras en el archivo")
    print("Digite 3 para Borrar el archivo x_x")
    print("Digite 4 para JUGAR")
    print("Digite 5 para ***SALIR***")
    opcion = ja.correcion()
    
    if(opcion==1):
        a.verificar()
    elif(opcion==2):
        a.llenar()
    elif(opcion==3):
        a.borrarArchivo()
    elif(opcion==4):
        if(not a.verificarVacio()):
            vectorcito=[]
            vectorcito = a.obtenerPalabrasArchivo()
            if(not vectorcito==1):
                ja.inicioAhorcado(ja.elegirPalabra(vectorcito))
        
        else:
            print("El archivo esta vacio!")
            print("LLenalo")
            a.llenar()
    elif(opcion==5):
        print("Gracias por jugar")

            


